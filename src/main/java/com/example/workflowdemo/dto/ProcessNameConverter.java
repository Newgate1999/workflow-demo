package com.example.workflowdemo.dto;

import com.example.workflowdemo.controller.ProcessName;

import java.util.Optional;

public class ProcessNameConverter {

    private static final String MESSAGE = "Process name supplied is not valid";

    public static ProcessName processNameJsonToDto(ProcessNameJson processNameJson) {
        return Optional.ofNullable(processNameJson)
                .map(ProcessNameJson::getProcessName)
                .filter(ProcessNameConverter::nonEmpty)
                .map(ProcessName::new)
                .orElseThrow(() -> new NullPointerException());
    }

    public static boolean nonEmpty(String processName) {
        return !processName.isEmpty();
    }
}
