package com.example.workflowdemo.dto;

import lombok.Builder;
import lombok.Data;
import org.activiti.api.process.model.payloads.StartProcessPayload;
import org.activiti.engine.task.Task;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@Builder
public class ProcessRequestDTO {

    private String taskId;
    private String type;
    private String status;
    private String processId;
    private String fileId;
    private String leadId;
    private String name;
    private String accNo;
    private String phoneMobile;
    private Object lead;
    private String check;
    private String contactId;
    private String accountId;
    private String campaignId;
    private String processDefinitionKey;
    private String statusPre;
    private int countLeadPreview;
    private Boolean isMB247;
    private Map<String, Object> condition;
    private List<Object> leads;
    private Map<String, String> dataAssign;
    private StartProcessPayload payload;
    private List<String> ids;
    private List<String> branches;
    private String assignProcessType;
    private Boolean checkDuplicate;

    private String rsId;
    private String scope;
    private String userName;
    private String hrsCode;
    private String branchCode;
    private Task taskModel;
    private Boolean isRm;
    private Boolean isFromHO;
    private Boolean isBranchFormFile;
    private Integer slaManager;
    private Integer slaRm;
    private Date startDate;
    private Date endDate;
}
