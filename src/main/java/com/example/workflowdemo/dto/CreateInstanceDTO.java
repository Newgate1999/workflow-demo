package com.example.workflowdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateInstanceDTO {
    private String processDefinitionKey;
    private HashMap<String, Object> variables;
}
