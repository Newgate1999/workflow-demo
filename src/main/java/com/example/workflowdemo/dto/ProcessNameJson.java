package com.example.workflowdemo.dto;

public class ProcessNameJson {
    private String processName;
    private String path;

    public ProcessNameJson() { }

    public ProcessNameJson(String processName) {
        this.processName = processName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getProcessName() {
        return processName;
    }
}
