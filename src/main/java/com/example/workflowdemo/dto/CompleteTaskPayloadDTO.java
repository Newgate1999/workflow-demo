package com.example.workflowdemo.dto;

import lombok.*;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompleteTaskPayloadDTO {

    private String id;
    private String taskId;
    private Map<String, Object> variables;
    private String processInstanceId;
    private String candidateGroup;
}
