package com.example.workflowdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.activiti.engine.task.DelegationState;

import java.util.Date;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO {
    private String id;
    private String name;
    private String assignee;
    private String description;
    private String executionId;
    private String owner;
    private String processInstanceId;
    private Date createTime;
    private String taskDefinitionKey;
    private Date dueDate;
    private String parentTaskId;
    private String tenantId;
    private Map<String, Object> taskLocalVariables;
    private Map<String, Object> processVariables;
    private String processDefinitionId;
    private DelegationState delegationState;
}
