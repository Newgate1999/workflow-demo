package com.example.workflowdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProcessInstanceDTO {
    private String processDefinitionId;
    private String processDefinitionKey;
    private String processDefinitionName;
    private Integer processDefinitionVersion;
    private String deploymentId;
    private String id;
    private Date startTime;
    private Integer suspensionState;
    private Integer eventSubscriptionCount;
    private Integer taskCount;
    private Integer jobCount;
    private Integer timerJobCount;
    private Integer suspendedJobCount;
    private Integer deadLetterJobCount;
    private Integer variableCount;
}
