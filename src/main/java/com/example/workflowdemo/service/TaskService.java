package com.example.workflowdemo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.engine.task.Task;

import java.util.List;


public interface TaskService {


    org.activiti.engine.task.Task getTaskById(
            boolean includeTaskLocalVariables, boolean includeProcessVariables, String taskId);

    void changeTask(CompleteTaskPayload payload);

    void createTaskBooking(CompleteTaskPayload payload) throws JsonProcessingException;

    void completeTask(String processInstanceId);

    void updateTask(Boolean bookingSuccess, String username, String status, String id);

    List<Task> findAllTasks();
}
