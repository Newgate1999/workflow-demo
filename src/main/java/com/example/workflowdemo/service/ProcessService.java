package com.example.workflowdemo.service;

import com.example.workflowdemo.controller.ProcessName;
import com.example.workflowdemo.dto.*;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.events.ProcessRuntimeEvent;
import org.activiti.engine.task.Task;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public interface ProcessService {

    /** @param event process event */
    void save(ProcessRuntimeEvent<ProcessInstance> event);

    /** service update status opportunity using process mb_crm_sale_process_case_... */
    void updateOpportunityStatus(String id, String status,String product)
            throws InterruptedException, ExecutionException;

    void createProcess(CreateInstanceDTO dto);

    StartProcessDTO startProcess();

    StartProcessDTO startProcess1();

    List<TaskDTO> findCurrentTask(String processInstanceId);

    void setVariables(UpdateTaskDTO dto);

    void completeTask(CompleteTaskPayloadDTO dto);

    List<TaskDTO> listCurrentTask(String processInstanceId);


    List<ProcessInstanceDTO> getAllProcess();


    ProcessInstanceDTO getProcessByProcessInstanceId(String processInstanceId);

    void endProcess();

    MarkResultDTO markPriority(String username, String phoneNumber);

    void sendMailPass();

    void deployProcess(ProcessName processName);

}
