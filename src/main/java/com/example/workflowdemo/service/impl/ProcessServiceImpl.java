package com.example.workflowdemo.service.impl;

import com.example.workflowdemo.controller.ProcessName;
import com.example.workflowdemo.dto.*;
import com.example.workflowdemo.service.ProcessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.model.events.ProcessRuntimeEvent;
import org.activiti.api.process.model.payloads.StartProcessPayload;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.process.runtime.events.ProcessUpdatedEvent;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.runtime.ProcessInstanceBuilder;
import org.activiti.engine.task.Task;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static java.lang.String.format;

@RequiredArgsConstructor
@Slf4j
@Service("ProcessService")
public class ProcessServiceImpl implements ProcessService {

    private final ProcessRuntime processRuntime;

    private final RuntimeService runtimeService;

    private final TaskService taskService;

    private final ModelMapper mapperForBuilder;

    private final ProcessEngine processEngine;

    @Override
    public void save(ProcessRuntimeEvent<org.activiti.api.process.model.ProcessInstance> event) {

        log.info("Catch event process: {}", event.toString());
    }

    @Override
    public void updateOpportunityStatus(String id, String status, String product) throws InterruptedException, ExecutionException {
        log.info("update opportunity status: {}, product: {}, id: {}", status, product, id);
    }

    @Override
    public void createProcess(CreateInstanceDTO dto) {

        ProcessInstanceBuilder builder = runtimeService.createProcessInstanceBuilder();
        if (dto.getVariables() != null && !dto.getVariables().isEmpty()) {
            builder.variables(dto.getVariables());
        }
        builder.processDefinitionKey(dto.getProcessDefinitionKey());
        ProcessInstance start = builder.start();
        log.info("createProcess success: {}", start.getProcessInstanceId());
    }

    @Override
    public StartProcessDTO startProcess() {

        log.info("Start process....");
        return StartProcessDTO.builder()
                .applicantName("anhpt10")
                .phoneNumber("0356027927")
                .build();
    }

    @Override
    public StartProcessDTO startProcess1() {
        log.info("Start process1....");
        return StartProcessDTO.builder()
                .applicantName("anhpt10")
                .phoneNumber("0356027927")
                .build();
    }

    @Override
    public List<TaskDTO> findCurrentTask(String processInstanceId) {
        var a = taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .list();
        return taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .list()
                .stream()
                .map(item ->
                        mapperForBuilder
                                .map(item, TaskDTO.TaskDTOBuilder.class)
                                .build())
                .collect(Collectors.toList());
    }

    @Override
    public void setVariables(UpdateTaskDTO dto) {

        dto.getVariables().put("result", Result.builder().satify(11).acceptInterview(true).build());
        var variables = dto.getVariables();

        if (variables.containsKey("satify") && variables.containsKey("acceptInterview")) {
            variables.put("result", Result.builder()
                    .satify((Integer) variables.get("satify"))
                    .acceptInterview((Boolean) variables.get("acceptInterview")).build());
        }
        runtimeService.setVariables(dto.getTaskId(), variables);
        taskService.complete(dto.getTaskId());
    }

    @Override
    public void completeTask(CompleteTaskPayloadDTO dto) {

        Task task = taskService.createTaskQuery()
                .taskId(dto.getTaskId())
//                .taskCandidateGroup("dev-managers")
                .singleResult();

        var variables = dto.getVariables();

        if (variables.containsKey("satify") && variables.containsKey("acceptInterview")) {
            variables.put("result", Result.builder()
                    .satify((Integer) variables.get("satify"))
                    .acceptInterview((Boolean) variables.get("acceptInterview"))
                    .build());
        }

        taskService.complete(task.getId(), variables);

    }

    @Override
    public List<TaskDTO> listCurrentTask(String processInstanceId) {
        List<TaskDTO> tasks = taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .orderByTaskName().asc()
                .list()
                .stream()
                .map(item ->
                        mapperForBuilder
                                .map(item, TaskDTO.TaskDTOBuilder.class)
                                .build())
                .collect(Collectors.toList());
        log.info("complete get current task size: {}", tasks.size());
        return tasks;
    }

    @Override
    public List<ProcessInstanceDTO> getAllProcess() {


        return runtimeService.createProcessInstanceQuery()
                .orderByProcessInstanceId()
                .desc()
                .list()
                .stream()
                .map(item ->
                        mapperForBuilder
                                .map(item, ProcessInstanceDTO.ProcessInstanceDTOBuilder.class)
                                .build())
                .collect(Collectors.toList());
    }

    @Override
    public ProcessInstanceDTO getProcessByProcessInstanceId(String processInstanceId) {
        var a =  runtimeService.createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
        return mapperForBuilder.map(a, ProcessInstanceDTO.ProcessInstanceDTOBuilder.class).build();
    }

    @Override
    public void endProcess() {
        log.info("send mail pass");
    }

    @Override
    public MarkResultDTO markPriority(String username, String phoneNumber) {
        log.info("markPriority date: {}", new Date());
        return MarkResultDTO.builder()
                .username(username)
                .phoneNumber(phoneNumber)
                .note("this is test save variable!!!")
                .build();
    }

    @Override
    public void sendMailPass() {

        log.info("send mail pass!!!");
    }

    @Override
    public void deployProcess(ProcessName processName) {

        Deployment deployment = processEngine.getRepositoryService()
                .createDeployment()
//                .addClasspathResource("processes/my-process.bpmn20.xml")
                .addClasspathResource(processName.getPath())
                .name(processName.getProcessName())
                .deploy();

        log.info(format("DEPLOYMENT ID: %s", deployment.getId()));
        log.info(format("DEPLOYMENT NAME: %s", deployment.getName()));
    }
}
