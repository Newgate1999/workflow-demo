package com.example.workflowdemo.service.impl;

import com.example.workflowdemo.Exception.RecordNotExistException;
import com.example.workflowdemo.service.ProcessService;
import com.example.workflowdemo.service.TaskService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.runtime.ProcessInstanceBuilder;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
@Slf4j
public class TaskServiceImpl implements TaskService {

    private final TaskRuntime taskRuntime;

    private final RuntimeService runtimeService;

    private final ProcessEngine processEngine;

    private final org.activiti.engine.TaskService taskService;

    @Override
    public Task getTaskById(boolean includeTaskLocalVariables, boolean includeProcessVariables, String taskId) {
        TaskQuery query = taskService.createTaskQuery().taskId(taskId);
        org.activiti.engine.task.Task task = query.singleResult();
        if (task == null) {
            throw new RecordNotExistException();
        }
        return task;
    }

    @Override
    public void changeTask(CompleteTaskPayload payload) {

        String processInstanceId;
        var opportunityId = (String) payload.getVariables().get("opportunityId");
        List<Task> taskActive = getTasksByOpportunityId(opportunityId);
        if (CollectionUtils.isNotEmpty(taskActive)) {
            Task task = taskActive.get(0);
            processInstanceId = task.getProcessInstanceId();
            log.info("Task already exists: {}", task.getProcessInstanceId());
        } else {
            HashMap<String, Object> lstVariablePayLoad = new HashMap<>();
            lstVariablePayLoad.put("opportunityId", opportunityId);
            lstVariablePayLoad.put("process", "mb_crm_sale_process_case_1");
            lstVariablePayLoad.put("process_sale_start", true);
            lstVariablePayLoad.put("product", payload.getVariables().get("product"));

            ProcessInstanceBuilder builder = runtimeService.createProcessInstanceBuilder();
            builder.variables(lstVariablePayLoad);
            builder.processDefinitionKey("mb_crm_sale_process_case_1");

            ProcessInstance start = builder.start();
            processInstanceId = start.getProcessInstanceId();
            log.info("Task start: {}", start.toString());
        }
        CompletableFuture.runAsync(
                () -> completeTaskOpportunity(processInstanceId, payload, "test product"));
    }

    @Override
    public void createTaskBooking(CompleteTaskPayload payload) throws JsonProcessingException {
        String processInstanceId;
//        String bookingId = (String) payload.getVariables().get("bookingId");
//        TaskQuery taskQuery = taskService.createTaskQuery();
//        taskQuery.taskVariableValueEquals("bookingId", bookingId);
//        var currentTask = taskQuery.includeTaskLocalVariables()
//                .includeTaskLocalVariables()
//                .orderByDueDateNullsLast()
//                .desc()
//                .list();
//
//        if (CollectionUtils.isEmpty(currentTask)) {
//
//        }

        HashMap<String, Object> lstVariablePayLoad = new HashMap<>();
        lstVariablePayLoad.put("process", "process_booking");
        lstVariablePayLoad.put("bookingId", payload.getVariables().get("bookingId"));
        ProcessInstanceBuilder processInstanceBuilder = runtimeService.createProcessInstanceBuilder();
        processInstanceBuilder.variables(lstVariablePayLoad);
        processInstanceBuilder.processDefinitionKey("process_booking");
        ProcessInstance start = processInstanceBuilder.start();
        processInstanceId = start.getProcessInstanceId();
        log.info("processInstanceId: " + processInstanceId);

    }

    @Override
    public void completeTask(String processInstanceId) {
        taskService.complete(processInstanceId);
    }

    @Override
    public void updateTask(Boolean bookingSuccess, String username, String status, String id) {
        TaskQuery query = taskService.createTaskQuery();
        query.taskVariableValueEquals("bookingId", id);
        includeVariables(query, true, true);
        List<Task> tasks =  query.orderByDueDateNullsLast().desc().list();
        HashMap<String, Object> lstVariablePayLoad = new HashMap<>();
        lstVariablePayLoad.put("booking_success", true);
        lstVariablePayLoad.put("status", status);
        lstVariablePayLoad.put("username", username);
        lstVariablePayLoad.put("process", "process_booking");
        lstVariablePayLoad.put("bookingId", id);
        taskService.setVariables(tasks.get(0).getId(), lstVariablePayLoad);
        taskService.complete(tasks.get(0).getId());
    }

    @Override
    public List<Task> findAllTasks() {
        return processEngine.getTaskService()
                .createTaskQuery()
                .list();
    }

    private static void includeVariables(
            @NonNull TaskQuery query,
            boolean includeTaskLocalVariables,
            boolean includeProcessVariables) {
        if (includeProcessVariables) query.includeProcessVariables();
        if (includeTaskLocalVariables) query.includeTaskLocalVariables();
    }

    public List<Task> getTasksByOpportunityId(String opportunityId) {
        TaskQuery query = taskService.createTaskQuery();
        query.taskVariableValueEquals("opportunityId", opportunityId);
        includeVariables(query, true, true);
        return query.orderByDueDateNullsLast().desc().list();
    }

    private void completeTaskOpportunity(
            String processInstanceId,
            CompleteTaskPayload payload,
            String productCodeSale) {

        Task task = getCurrentTask(true, true, processInstanceId);
        Map<String, Object> variables = taskService.getVariables(task.getId());
        Map<String, Object> variablesNew = new HashMap<>();
        Object processBpmn = variables.get("process");
        Object processSaleStart = variables.get("process_sale_start");
        if (processBpmn != null && processSaleStart != null && Boolean.parseBoolean(processSaleStart.toString())) {
            variablesNew.put("opportunityId", variables.get("opportunityId"));
            variablesNew.put("process", variables.get("process"));
            variablesNew.put("process_sale_start", false);

            variables = variablesNew;
        }

        Map<String, Object> listStatus = new HashMap<>();
        listStatus.put("status_working", false);
        listStatus.put("status_won", false);
        listStatus.put("status_new", false);
        listStatus.put("status_lost", false);
        listStatus.put("status_handling_records", false);
        listStatus.put("status_closing_sales", false);

        List<String> statusPer = payload.getVariables().keySet().stream()
                .filter(o -> o.startsWith("status_"))
                .collect(Collectors.toList());
        if (!statusPer.isEmpty()) {
            variables.put("status", statusPer.get(0).replace("status_", "").toUpperCase());
            listStatus.put(statusPer.get(0), true);
        }
        variables.putAll(listStatus);
        variables.putAll(payload.getVariables());
        variables.put("disabled", false);
        variables.put("product", productCodeSale);
        taskService.setVariables(task.getId(), variables);
        List<Task> task1 = taskService.createTaskQuery().taskId(task.getId()).list();
        log.info(task1.toString());
        taskService.complete(task.getId());
    }

    public Task getCurrentTask(
            boolean includeTaskLocalVariables,
            boolean includeProcessVariables,
            String processInstanceId) {
        TaskQuery query = taskService.createTaskQuery().processInstanceId(processInstanceId);
        includeVariables(query, includeTaskLocalVariables, includeProcessVariables);
        return query.singleResult();
    }
}
