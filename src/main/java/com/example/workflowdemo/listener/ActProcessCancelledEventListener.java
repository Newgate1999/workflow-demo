package com.example.workflowdemo.listener;

import com.example.workflowdemo.configuration.ApplicationContextHolder;
import com.example.workflowdemo.service.ProcessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.runtime.events.ProcessCancelledEvent;
import org.activiti.api.process.runtime.events.listener.ProcessRuntimeEventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ActProcessCancelledEventListener
    implements ProcessRuntimeEventListener<ProcessCancelledEvent> {

  @Override
  public void onEvent(ProcessCancelledEvent event) {
    ApplicationContextHolder.getBean(ProcessService.class).save(event);
  }
}
