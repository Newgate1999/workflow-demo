package com.example.workflowdemo.listener;

import com.example.workflowdemo.configuration.ApplicationContextHolder;
import com.example.workflowdemo.service.ProcessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.runtime.events.ProcessUpdatedEvent;
import org.activiti.api.process.runtime.events.listener.ProcessRuntimeEventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ActProcessUpdatedEventListener implements ProcessRuntimeEventListener<ProcessUpdatedEvent> {

    @Override
    public void onEvent(ProcessUpdatedEvent processUpdatedEvent) {
        ApplicationContextHolder.getBean(ProcessService.class).save(processUpdatedEvent);
    }
}
