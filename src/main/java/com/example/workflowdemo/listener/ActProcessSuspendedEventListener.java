package com.example.workflowdemo.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.runtime.events.ProcessSuspendedEvent;
import org.activiti.api.process.runtime.events.listener.ProcessRuntimeEventListener;
import org.springframework.stereotype.Component;
import com.example.workflowdemo.configuration.ApplicationContextHolder;
import com.example.workflowdemo.service.ProcessService;

@Component
@Slf4j
@RequiredArgsConstructor
public class ActProcessSuspendedEventListener
    implements ProcessRuntimeEventListener<ProcessSuspendedEvent> {

  @Override
  public void onEvent(ProcessSuspendedEvent event) {
    ApplicationContextHolder.getBean(ProcessService.class).save(event);
  }
}
