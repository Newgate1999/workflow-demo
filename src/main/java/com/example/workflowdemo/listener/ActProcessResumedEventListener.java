package com.example.workflowdemo.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.runtime.events.ProcessResumedEvent;
import org.activiti.api.process.runtime.events.listener.ProcessRuntimeEventListener;
import org.springframework.stereotype.Component;
import com.example.workflowdemo.configuration.ApplicationContextHolder;
import com.example.workflowdemo.service.ProcessService;

@Component
@Slf4j
@RequiredArgsConstructor
public class ActProcessResumedEventListener
    implements ProcessRuntimeEventListener<ProcessResumedEvent> {

  @Override
  public void onEvent(ProcessResumedEvent event) {
    ApplicationContextHolder.getBean(ProcessService.class).save(event);
  }
}
