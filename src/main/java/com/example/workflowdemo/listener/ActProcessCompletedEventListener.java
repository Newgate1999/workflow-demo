package com.example.workflowdemo.listener;

import com.example.workflowdemo.configuration.ApplicationContextHolder;
import com.example.workflowdemo.service.ProcessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.runtime.events.ProcessCompletedEvent;
import org.activiti.api.process.runtime.events.listener.ProcessRuntimeEventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ActProcessCompletedEventListener
    implements ProcessRuntimeEventListener<ProcessCompletedEvent> {

  @Override
  public void onEvent(ProcessCompletedEvent event) {
    ApplicationContextHolder.getBean(ProcessService.class).save(event);
  }
}
