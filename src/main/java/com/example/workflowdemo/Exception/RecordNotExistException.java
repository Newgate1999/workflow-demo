package com.example.workflowdemo.Exception;

public class RecordNotExistException extends ApplicationException {
    public RecordNotExistException() {
        super("Record not exist");
    }
}
