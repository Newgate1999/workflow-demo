package com.example.workflowdemo.controller;

import com.example.workflowdemo.dto.*;
import com.example.workflowdemo.service.ProcessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("/process")
@RequiredArgsConstructor
@Slf4j
public class ProcessController {

    private final ProcessService processService;

    @PostMapping("/create")
    public ResponseEntity<Response> createProcess(@RequestBody CreateInstanceDTO dto) {
        processService.createProcess(dto);
        Response response = new Response();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/task/{processInstanceId}")
    public List<TaskDTO> listTaskByProcessId(@PathVariable String processInstanceId) {
        return processService.findCurrentTask(processInstanceId);
    }

    @PostMapping("/update")
    public void updateTask(UpdateTaskDTO dto) {
        processService.setVariables(dto);
    }

    @PostMapping("/complete")
    public void completeTask(@RequestBody CompleteTaskPayloadDTO completeTaskPayload) {
        processService.completeTask(completeTaskPayload);
    }

    @GetMapping("/check-process/{processInstanceId}")
    public List<TaskDTO> getCurrentTasks(@PathVariable String processInstanceId) {
        return processService.listCurrentTask(processInstanceId);
    }

    @GetMapping("/get-all")
    public List<ProcessInstanceDTO> getAllProcess() {
        return processService.getAllProcess();
    }

    @GetMapping("/info/{id}")
    public ProcessInstanceDTO getProcessByInstanceId(@PathVariable String id) {
        return processService.getProcessByProcessInstanceId(id);
    }

    @PostMapping(value = "/deploy", produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = OK)
    public void deploy(@RequestBody ProcessNameJson processNameJson) {
        ProcessName processName = new ProcessName(processNameJson.getProcessName(), processNameJson.getPath());
        processService.deployProcess(processName);
    }

    public static ProcessName processNameJsonToDto(ProcessNameJson processNameJson) {
        return Optional.ofNullable(processNameJson)
                .map(ProcessNameJson::getProcessName)
                .filter(ProcessNameConverter::nonEmpty)
                .map(ProcessName::new)
                .orElseThrow(() -> new NullPointerException());
    }
}
