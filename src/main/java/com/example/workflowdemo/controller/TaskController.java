package com.example.workflowdemo.controller;

import com.example.workflowdemo.dto.CompleteTaskPayloadDTO;
import com.example.workflowdemo.dto.Response;
import com.example.workflowdemo.service.TaskService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.engine.task.Task;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/tasks")
@RequiredArgsConstructor
@Slf4j
public class TaskController {

    private final TaskService taskService;

    @PostMapping("/complete-task")
    public ResponseEntity<Response> completeTask(@RequestBody CompleteTaskPayload payload) {
        taskService.changeTask(payload);
        Response response = new Response();
        return ResponseEntity.ok(response);
    }

    @PostMapping("/craete-task-booking")
    public ResponseEntity<Response> createBooking(@RequestBody CompleteTaskPayload payload) throws JsonProcessingException {
        taskService.createTaskBooking(payload);
        Response response = new Response();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/complete-task-booking/{id}")
    public ResponseEntity<Response> completeBooking(@PathVariable String id) {
        taskService.completeTask(id);
        Response response = new Response();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/update-task-booking")
    public ResponseEntity<Response> updateBooking(@RequestParam String id,
                                                  @RequestParam Boolean bookingSuccess,
                                                  @RequestParam String username,
                                                  @RequestParam String status) {
        taskService.updateTask(bookingSuccess, username, status, id);
        Response response = new Response();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/tasks")
    public List<Task> getAllTasks() {
        return taskService.findAllTasks();
    }
}
