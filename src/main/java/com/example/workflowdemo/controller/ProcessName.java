package com.example.workflowdemo.controller;

import java.util.Objects;

public class ProcessName {
    private String processName;

    private String path;

    public ProcessName() { }

    public ProcessName(String processName, String path) {
        this.processName = processName;
        this.path = path;
    }

    public ProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessName() {
        return processName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessName that = (ProcessName) o;
        return Objects.equals(processName, that.processName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processName);
    }
}
