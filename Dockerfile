FROM openjdk:11

# Add a volume pointing to /tmp
VOLUME /tmp

# Add argument spring profile
ARG ENV_PROFILE
ENV ENV_PROFILE=$ENV_PROFILE

# Add the application's jar to the container
ADD /target/*.jar app.jar

ENTRYPOINT [ \
  "java", \
  "-server", \
  "-XX:+UseStringDeduplication", \
  "-Djava.security.egd=file:/dev/./urandom", \
  "-Dspring.security.strategy=MODE_INHERITABLETHREADLOCAL", \
  "-jar", \
  "app.jar" \
]

